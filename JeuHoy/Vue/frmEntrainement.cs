﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Forms;
using Microsoft.Kinect;
using Perceptron.Application.Commands.Train;
using Perceptron.Application.Queries.FindBestMatches;

namespace JeuHoy.Vue
{
    /// <summary>
    ///     Auteur: Hugo St-Louis
    ///     Description: Permet de faire l'entrainement des différentes figures de danse.
    ///     Date: 26/04/2014
    /// </summary>
    public partial class frmEntrainement : Form
    {
        private readonly Bitmap _bmapSquelette;
        private readonly Dictionary<string, Image> _dicImgFigure = new Dictionary<string, Image>();
        private readonly KinectSensor _sensor;
        private Skeleton _skeleton;
        private readonly JouerSon _son = new JouerSon();
        private bool _isClosing;
        private int _positionEnCours = 1;

        private readonly ITrainCommand _trainCommand;
        private readonly IFindBestMatchesQuery _findBestMatchesQuery;

        /// <summary>
        ///     Constructeur
        /// </summary>
        public frmEntrainement(ITrainCommand trainCommand, IFindBestMatchesQuery findBestMatchesQuery)
        {
            _trainCommand = trainCommand;
            _findBestMatchesQuery = findBestMatchesQuery;
            InitializeComponent();

            picKinect.Width = CstApplication.KINECT_DISPLAY_WIDTH;
            picKinect.Height = CstApplication.KINECT_DISPLAY_HEIGHT;
            shapeContainer1.BringToFront();
            for (var i = 1; i <= CstApplication.NBFIGURE; i++)
                _dicImgFigure.Add("fig" + i, Image.FromFile(@"./HoyContent/fig" + i + ".png"));

            _bmapSquelette = new Bitmap(pDessinSquelette.Width, pDessinSquelette.Height);

            lblNbPosition.Text = CstApplication.NBFIGURE.ToString();
            ChargerFigure();
            _son.JouerSonAsync(@"./HoyContent/hoy.wav");

            if (KinectSensor.KinectSensors.Count <= 0)
                return;

            _sensor = KinectSensor.KinectSensors[0];

            if (_sensor.Status != KinectStatus.Connected)
                return;

            _sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

            _sensor.SkeletonStream.Enable();
            _sensor.AllFramesReady += sensor_AllFramesReady;

            try
            {
                _sensor.Start();
                _sensor.ElevationAngle = 0;
            }
            catch (IOException)
            {
                MessageBox.Show(@"Impossible de se connecter à la kinect! Vérifier les branchements");
            }

            timer = new System.Timers.Timer(10000);
            timer.Elapsed += new ElapsedEventHandler(Timer_Interval);
            timer.Interval = 500;
            timer.Enabled = true;
        }

        /// <summary>
        /// Initialise tous les sensors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            //Affiche l'image de la caméra couleur dans un picturebox
            using (var colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame != null)
                {
                    var pixels = new byte[colorFrame.PixelDataLength];
                    colorFrame.CopyPixelDataTo(pixels);
                    var imgKinectCouleur = CreerBitMapAPartirPixels(pixels, colorFrame.Height, colorFrame.Width);
                    picKinect.Image = imgKinectCouleur;
                }
            }

            if (e.OpenDepthImageFrame() == null)
                return;

            using (var skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                    return;

                var allSkeleton = new Skeleton[6];
                skeletonFrameData.CopySkeletonDataTo(allSkeleton);

                var joueur = (from s in allSkeleton
                              where s.TrackingState == SkeletonTrackingState.Tracked
                              select s).FirstOrDefault();

                if (joueur == null)
                    return;

                _skeleton = joueur;
                DessinerSquelette();
            }
        }

        /// <summary>
        ///     Dessine un ellipse pour chacune des jointure du squelette détecté.
        /// </summary>
        private void DessinerSquelette()
        {
            try
            {
                if (_isClosing || _skeleton == null)
                    return;

                using (var g = Graphics.FromImage(_bmapSquelette))
                {
                    g.Clear(Color.Black);
                }

                for (var i = 1; i < _skeleton.Joints.Count; i++)
                {
                    var iCoordY = Height - ClientSize.Height;
                    var iCoordX = (Width - ClientSize.Width) / 2;
                    var point = _sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(
                        _skeleton.Joints[(JointType)i].Position,
                        DepthImageFormat.Resolution640x480Fps30);

                    var y = (float)(point.Y + iCoordY) / 2;
                    var x = (float)(point.X - iCoordX) / 2;

                    using (var g = Graphics.FromImage(_bmapSquelette))
                    {
                        g.FillEllipse(Brushes.White, x, y, 10, 10);
                    }
                    pDessinSquelette.Invalidate();
                }
            }
            catch (Exception ex)
            {
                txtConsole.Text = ex.Message;
            }
        }

        /// <summary>
        ///     Crée un Bitmap à partir d'un vecteur de pixels en format RGBA.
        /// </summary>
        /// <param name="pixels">un vecteur de pixels en format RGBA</param>
        /// <param name="hauteur">Hauteur de l'image</param>
        /// <param name="largeur">Largeur de l'image</param>
        /// <returns></returns>
        private static Bitmap CreerBitMapAPartirPixels(byte[] pixels, int hauteur, int largeur)
        {
            //Crée un bitmap vide du bon type pour la bonne dimension
            var bitmapFrame = new Bitmap(largeur, hauteur, PixelFormat.Format32bppRgb);

            //Réserve un espace mémoire de type bitmapData pour les pixels
            var bitmapData = bitmapFrame.LockBits(new Rectangle(0, 0, largeur, hauteur), ImageLockMode.WriteOnly,
                bitmapFrame.PixelFormat);

            //Transfert les pixels dans l'espace mémoire réservé.
            var intPointer = bitmapData.Scan0;
            Marshal.Copy(pixels, 0, intPointer, pixels.Length);
            //Active l'espace mémoire réservé de pixels pour remplir le Bitmap
            bitmapFrame.UnlockBits(bitmapData);

            return bitmapFrame;
        }

        /// <summary>
        ///     Ferme la fenêtre lorsqu'on appuie sur le bouton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFermer_Click(object sender, EventArgs e)
        {
            _isClosing = true;
            Close();
        }

        /// <summary>
        ///     Charger la figure de danse en cours.
        /// </summary>
        private void ChargerFigure()
        {

            if (_positionEnCours > CstApplication.NBFIGURE)
                _positionEnCours = 1;

            if (_positionEnCours < 1)
                _positionEnCours = CstApplication.NBFIGURE;

            lblFigureEnCours.Text = _positionEnCours.ToString();

            var bResultat = _dicImgFigure.TryGetValue("fig" + _positionEnCours, out Image imgValue);
            if (bResultat)
                picPositionAFaire.Image = imgValue;
        }

        /// <summary>
        /// Timer pour identifier la position en temps réel
        /// </summary>
        private static System.Timers.Timer timer;

        /// <summary>
        ///     Lorsqu'on appuie sur le bouton suivant ou précédent, modifier la figure en conséquence.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClickChangerFigure_Click(object sender, EventArgs e)
        {
            var bouton = (Control)sender;

            switch (bouton.Name)
            {
                case "btnSuivant":
                    _positionEnCours++;
                    break;
                case "btnPrecedent":
                    _positionEnCours--;
                    break;
                default:
                    break;
            }

            ChargerFigure();
        }

        /// <summary>
        /// fait apprendre la position au perceptron
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApprendre_Click(object sender, EventArgs e)
        {
            if (_skeleton != null)
            {
                _trainCommand.Execute(new TrainingModel($"fig{_positionEnCours}", _skeleton, 0.1f));

                DisplayFoundMatch();
            }
        }
        
        /// <summary>
        /// Lance une method par interval
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Interval(object sender, ElapsedEventArgs e)
        {
            DisplayFoundMatch();
        }

        private void DisplayFoundMatch()
        {
            if (_skeleton != null)
            {
                try
                {
                    var match = _findBestMatchesQuery.Execute(_skeleton).First();

                    txtConsole.Text += $"Figure trouvée: {match.FoundedName} \r\n" +
                                       $@"Force: {match.Strength}";
                }
                catch (Exception e)
                {
                }
            }
        }

        /// <summary>
        ///     Dessiner les points de jointure sur le panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pDessinSquelette_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(_bmapSquelette, Point.Empty);
        }

        /// <summary>
        ///     Fermeture de la fenêtre.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picRetour_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        ///     Change le curseur lorsque le curseur est sur l'image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picRetour_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        /// <summary>
        ///     Change le curseur lorsque le curseur est sur l'image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picRetour_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Arrow;
        }
    }
}