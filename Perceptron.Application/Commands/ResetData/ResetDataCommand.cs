﻿using Perceptron.Persistence;

/// <summary>
/// Auteur: Francois Brisson, Maxime Gélinas
/// Date: 2017/04/21
/// Description: Réinitialise le data
/// </summary>
namespace Perceptron.Application.Commands.ResetData
{
    public class ResetDataCommand : IResetDataCommand
    {
        private readonly IBoundaryRepository _boundaryRepository;
        private readonly ILearningSampleRepository _learningSampleRepository;

        /// <summary>
        /// Constructeur pour la réinitialisation du data
        /// </summary>
        /// <param name="boundaryRepository"></param>
        /// <param name="learningSampleRepository"></param>
        public ResetDataCommand(IBoundaryRepository boundaryRepository, ILearningSampleRepository learningSampleRepository)
        {
            _boundaryRepository = boundaryRepository;
            _learningSampleRepository = learningSampleRepository;
        }

        /// <summary>
        /// Exécute la réinitialisaton du data
        /// </summary>
        public void Execute()
        {
            _boundaryRepository.Clear();
            _learningSampleRepository.Clear();

            _boundaryRepository.Save();
            _learningSampleRepository.Save();
        }
    }
}
