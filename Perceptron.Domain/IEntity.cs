﻿using System;

namespace Perceptron.Domain
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Interface des entité qui détiennent un ID guid
    /// </summary>
    public interface IEntity
    {
        Guid Id { get; }
    }
}