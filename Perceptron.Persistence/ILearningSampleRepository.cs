﻿using Perceptron.Domain;

namespace Perceptron.Persistence
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Interface des échantillons d'apprentissage 
    /// </summary>
    public interface ILearningSampleRepository : 
        IRepository<LearningSample>
    {
    }
}
