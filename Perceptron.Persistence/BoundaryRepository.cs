﻿using System;
using System.Linq;
using Perceptron.Domain;

namespace Perceptron.Persistence
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Repository des droite de boundary
    /// </summary>
    public class BoundaryRepository : Repository<Boundary>,
        IBoundaryRepository
    {
        /// <summary>
        ///     Constructeur du repository des boundaries
        /// </summary>
        /// <param name="storage"></param>
        public BoundaryRepository(IStorage<Boundary> storage) :
            base(storage)
        {
        }

        /// <summary>
        ///     Vérifie si une boundary existe
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public bool Exists(Func<Boundary, bool> predicate) =>
            Storage
                .GetAll()
                .Any(predicate);

        /// <summary>
        ///     Cherche une boundary avec un nom
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Boundary GetByName(string name) =>
            Storage
                .GetAll()
                .Single(boundary => boundary.Name == name);
    }
}
