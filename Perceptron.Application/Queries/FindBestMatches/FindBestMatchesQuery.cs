﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Kinect;
using Perceptron.Domain;
using Perceptron.Persistence;

namespace Perceptron.Application.Queries.FindBestMatches
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Trouve les meilleur match avec les charactères entraînés et le dessin
    /// </summary>
    public class FindBestMatchesQuery : IFindBestMatchesQuery
    {
        private readonly IBoundaryRepository _boundaryRepository;

        public FindBestMatchesQuery(IBoundaryRepository boundaryRepository) =>
            _boundaryRepository = boundaryRepository;

        public IEnumerable<MatchModel> Execute(Skeleton skeleton)
        {
            if (_boundaryRepository.GetAll().Any())
                return _boundaryRepository
                    .GetAll()
                    .Select(boundary =>
                        new MatchModel(boundary.Name,
                            boundary.Evaluate(LearningSample.CreateNew(boundary.Name, skeleton))))
                    .Where(match => match.Strength > 0)
                    .OrderByDescending(match => match.Strength);
            return Enumerable.Empty<MatchModel>();
        }
    }
}
