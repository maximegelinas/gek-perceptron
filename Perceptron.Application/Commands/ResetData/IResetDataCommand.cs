﻿namespace Perceptron.Application.Commands.ResetData
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Interface pour réinitialiser le data
    /// </summary>
    public interface IResetDataCommand
    {
        void Execute();
    }
}