﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Perceptron.Domain
{
    /// <summary>
    ///     Auteur: Francois Brisson, Maxime Gélinas
    ///     Date: 2017/04/21
    ///     Description: Droite de limite pour les tests et l'entrainement
    /// </summary>
    public class Boundary : Entity
    {
        /// <summary>
        ///     Constructeur des boundary
        /// </summary>
        /// <param name="name"></param>
        /// <param name="synapticWeights"></param>
        private Boundary(string name, IEnumerable<float> synapticWeights)
        {
            Name = name;
            SynapticWeights = synapticWeights.ToList();
        }

        private Boundary()
        {
        }

        public string Name { get; protected set; }

        // TODO(maximegelinas): Make it read only without breaking persistence.
        public List<float> SynapticWeights { get; protected set; }

        /// <summary>
        ///     Créer une nouvelle boundary
        /// </summary>
        /// <param name="name"></param>
        /// <param name="firstLearningSample"></param>
        /// <returns></returns>
        public static Boundary CreateNew(
            string name, LearningSample firstLearningSample) =>
            new Boundary(
                name,
                firstLearningSample.Values);

        /// <summary>
        ///     Évaluer si un dessin fait parti de la boundary 
        /// </summary>
        /// <param name="learningSample"></param>
        /// <returns></returns>
        public float Evaluate(LearningSample learningSample)
        {
            if (learningSample?.Values == null || SynapticWeights == null)
                return 0;

            if (learningSample.Values.Length != SynapticWeights.Count)
                throw new ArgumentException();

            return SynapticWeights
                       .Zip(
                    learningSample.Values,
                           (synapticWeight, learningSampleValue) =>
                               synapticWeight * learningSampleValue)
                       .Sum();
        }

        /// <summary>
        ///     Update les poids synaptiques des boundary
        /// </summary>
        /// <param name="learningSample"></param>
        /// <param name="learningConstant"></param>
        public void Update(LearningSample learningSample, float learningConstant)
        {
            if (learningSample.Values == null ||
                SynapticWeights == null)
                return;

            if (learningSample.Values.Length != SynapticWeights.Count)
                throw new ArgumentException();

            SynapticWeights =
                SynapticWeights
                    .Zip(
                        learningSample.Values,
                        (synapticWeight, learningSampleValue) =>
                            synapticWeight + learningConstant * CalculateErrorCoefficient(learningSample) * learningSampleValue)
                    .ToList();
        }


        /// <summary>
        ///     Calcule le coefficient d'erreur pour la formule d'ajustement des poids synaptiques
        /// </summary>
        /// <param name="learningSample"></param>
        /// <returns></returns>
        private int CalculateErrorCoefficient(LearningSample learningSample) =>
            learningSample.Name == Name
                ? 1
                : 0 -
                  SynapticWeights
                      .Zip(
                          learningSample.Values,
                          (synapticWeight, learningSampleValue) =>
                              synapticWeight * learningSampleValue)
                      .Sum() > 0
                    ? 1
                    : 0;
    }
}