﻿using System;
using Perceptron.Domain;

namespace Perceptron.Persistence
{
    /// <summary>
    ///     Auteur: Francois Brisson, Maxime Gélinas
    ///     Date: 2017/04/21
    ///     Description: Interface des repository des droites calculés(boundary)
    /// </summary>
    public interface IBoundaryRepository :
        IRepository<Boundary>
    {
        bool Exists(Func<Boundary, bool> predicate);

        Boundary GetByName(string name);
    }
}