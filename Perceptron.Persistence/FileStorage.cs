﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Perceptron.Domain;

/// <summary>
/// Auteur: Francois Brisson, Maxime Gélinas
/// Date: 2017/04/21
/// Description: Permet la persistence dans un fichier
/// </summary>
namespace Perceptron.Persistence
{
    public class FileStorage<T> :
        IStorage<T>
        where T : IEntity
    {
        /// <summary>
        /// Constructeur de file storage
        /// </summary>
        /// <param name="fileName"></param>
        public FileStorage(string fileName)
        {
            FileName = fileName;
            LazyObjects =
                new Lazy<List<T>>(() =>
                    File.Exists(FileName)
                        ? File.ReadAllLines(FileName)
                            .Where(line => line.Trim() != "")
                            .Select(JsonConvert.DeserializeObject<T>)
                            .ToList()
                        : new List<T>());
        }

        private string FileName { get; }

        private Lazy<List<T>> LazyObjects { get; }

        private List<T> Objects =>
            LazyObjects.Value;

        public void Dispose()
        {
        }

        /// <summary>
        /// Ajoute un objet dans le storage
        /// </summary>
        /// <param name="obj"></param>
        public void Add(T obj) =>
            Objects.Add(obj);

        /// <summary>
        /// Vide le storage
        /// </summary>
        public void Clear() =>
            Objects.Clear();

        /// <summary>
        /// Obtient tous les objets sauvés dans le fichier
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll() =>
            Objects;

        /// <summary>
        /// Sauvegarde dans le fichier
        /// </summary>
        public void Save() =>
            File.WriteAllLines(
                FileName,
                Objects
                    .Select(obj =>
                        JsonConvert.SerializeObject(obj)));

        /// <summary>
        /// Update els objets dans le fichier
        /// </summary>
        /// <param name="obj"></param>
        public void Update(T obj)
        {
            var storedObject =
                Objects.Single(@object => @object.Id.Equals(obj.Id));

            Objects.Remove(storedObject);
            Objects.Add(obj);
        }
    }
}