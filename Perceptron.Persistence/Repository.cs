﻿using System;
using System.Collections.Generic;
using System.Linq;
using Perceptron.Domain;

namespace Perceptron.Persistence
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Est un repository des entity
    /// </summary>
    public abstract class Repository<T> : 
        IRepository<T>
        where T : IEntity
    {
        protected Repository(IStorage<T> storage) =>
            Storage = storage;

        public bool IsDisposed { get; private set; }

        protected IStorage<T> Storage { get; }

        /// <summary>
        /// ajoute un objet au storage
        /// </summary>
        /// <param name="obj"></param>
        public void Add(T obj) =>
            Storage.Add(obj);

        /// <summary>
        /// vide le storage
        /// </summary>
        public void Clear() =>
            Storage.Clear();

        public void Dispose()
        {
            Storage.Dispose();
            IsDisposed = true;
        }

        /// <summary>
        /// Obtient tous les objets dans le storage
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll() =>
            Storage.GetAll();

        /// <summary>
        /// Obtenir un objet par ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(Guid id) =>
            Storage.GetAll().First(obj => obj.Id == id);

        /// <summary>
        /// Sauvegarder les objets dans les storage
        /// </summary>
        public void Save() =>
            Storage.Save();
    }
}