﻿using System;
using System.Collections.Generic;
using Perceptron.Domain;

namespace Perceptron.Persistence
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Interface de repository d'entity
    /// </summary>
    public interface IRepository<T>
        where T : IEntity
    {
        bool IsDisposed { get; }

        void Add(T obj);

        void Clear();

        void Dispose();

        IEnumerable<T> GetAll();

        T GetById(Guid id);

        void Save();
    }
}