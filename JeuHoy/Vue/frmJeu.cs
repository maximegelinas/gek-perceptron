﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Kinect;

namespace JeuHoy.Vue
{
    /// <summary>
    /// Auteur: Hugo St-Louis
    /// Description: Fenêtre du jeu.
    /// Date: 26/04/2014
    /// </summary>
    public partial class frmJeu : Form
    {
        private readonly Dictionary<string, Image> _dicImgFigure = new Dictionary<string, Image>();
        private readonly JouerSon _son = new JouerSon();
        private int _positionEnCours = 1;

        /// <summary>
        ///     Constructeur
        /// </summary>
        public frmJeu()
        {
            InitializeComponent();

            shapeContainer1.BringToFront();
            for (var i = 1; i <= CstApplication.NBFIGURE; i++)
                _dicImgFigure.Add("fig" + i, Image.FromFile(@"./HoyContent/fig" + i + ".png"));

            lblNbPosition.Text = CstApplication.NBFIGURE.ToString();
            ChargerFigure();
            _son.JouerSonAsync(@"./HoyContent/hoy.wav");

            if (KinectSensor.KinectSensors.Count <= 0)
                return;

            var sensor = KinectSensor.KinectSensors[0];

            if (sensor.Status != KinectStatus.Connected)
                return;

            sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

            sensor.SkeletonStream.Enable();
            sensor.AllFramesReady += sensor_AllFramesReady;

            try
            {
                sensor.Start();
                sensor.ElevationAngle = 0;
            }
            catch (IOException)
            {
                MessageBox.Show(@"Impossible de se connecter à la kinect! Vérifier les branchements");
            }
        }

        private void sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            using (var colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame != null)
                {
                    var pixels = new byte[colorFrame.PixelDataLength];
                    colorFrame.CopyPixelDataTo(pixels);
                }
            }

            if (e.OpenDepthImageFrame() == null)
                return;

            //Détecte le squelette du joueur en cours et déplace les picturebox des mains et de la tête en conséquence.
            using (var skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                    return;

                var allSkeleton = new Skeleton[6];
                skeletonFrameData.CopySkeletonDataTo(allSkeleton);

                var joueur = (from s in allSkeleton
                    where s.TrackingState == SkeletonTrackingState.Tracked
                    select s).FirstOrDefault();

                if (joueur != null)
                    return;
            }
        }

        /// <summary>
        ///     Charger la figure de danse en cours.
        /// </summary>
        private void ChargerFigure()
        {

            if (_positionEnCours > CstApplication.NBFIGURE)
                _positionEnCours = 1;

            if (_positionEnCours < 1)
                _positionEnCours = CstApplication.NBFIGURE;

            lblFigureEnCours.Text = _positionEnCours.ToString();

            var bResultat = _dicImgFigure.TryGetValue("fig" + _positionEnCours, out Image imgValue);
            if (bResultat)
                picPositionAFaire.Image = imgValue;
        }

        /// <summary>
        /// Ferme la fenêtre lorsqu'on appuie sur le bouton retour.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Change le curseur lorsque le curseur est sur l'image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picRetour_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        /// <summary>
        /// Change le curseur lorsque le curseur nest plus sur l'image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picRetour_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

    }
}
