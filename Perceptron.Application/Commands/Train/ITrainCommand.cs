﻿namespace Perceptron.Application.Commands.Train
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Interface pour exécuter l'entraînement le perceptron
    /// </summary>
    public interface ITrainCommand
    {
        void Execute(TrainingModel model);
    }
}