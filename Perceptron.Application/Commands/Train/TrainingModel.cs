﻿using System.Collections;
using Microsoft.Kinect;

namespace Perceptron.Application.Commands.Train
{
    /// <summary>
    ///     Auteur: Francois Brisson, Maxime Gélinas
    ///     Date: 2017/04/21
    ///     Description: Model d'entrainement pour les dessins
    /// </summary>
    public class TrainingModel
    {
        /// <summary>
        ///     Constructeur du modèle d'entraînement
        /// </summary>
        /// <param name="name"></param>
        /// <param name="skeleton"></param>
        /// <param name="learningConstant"></param>
        public TrainingModel(string name, Skeleton skeleton, float learningConstant)
        {
            Name = name;
            Skeleton = skeleton;
            LearningConstant = learningConstant;
        }

        public Skeleton Skeleton { get; }

        public string Name { get; }

        public float LearningConstant { get; }
    }
}