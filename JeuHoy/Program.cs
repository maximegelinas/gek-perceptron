﻿using System;
using System.Windows.Forms;
using JeuHoy.Vue;
using Perceptron.Application.Commands.Train;
using Perceptron.Application.Queries.FindBestMatches;
using Perceptron.Domain;
using Perceptron.Persistence;

namespace JeuHoy
{
    internal static class Program
    {
        /// <summary>
        ///     Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            var boundaryStorage = new FileStorage<Boundary>("boundary.storage");
            var learningSampleStorage = new FileStorage<LearningSample>("learning-sample.storage");
            var learningSampleRepository = new LearningSampleRepository(learningSampleStorage);
            var boundaryRepository = new BoundaryRepository(boundaryStorage);
            var trainCommand = new TrainCommand(learningSampleRepository, boundaryRepository);
            var findBestMatchesQuery = new FindBestMatchesQuery(boundaryRepository);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmEntree(trainCommand, findBestMatchesQuery));
        }
    }
}
