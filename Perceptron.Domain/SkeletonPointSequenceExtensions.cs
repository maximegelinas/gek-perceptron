﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Kinect;

namespace Perceptron.Domain
{
    internal static class SkeletonPointSequenceExtensions
    {
        public static IEnumerable<SkeletonPoint> ToRelativePoints(
            this IEnumerable<SkeletonPoint> points, SkeletonPoint origin) =>
            points
                .Select(point => new SkeletonPoint
                {
                    X = origin.X - point.X,
                    Y = origin.Y - point.Y,
                    Z = origin.Z - point.Z
                });
    }
}