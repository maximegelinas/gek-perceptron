﻿using System;
using System.Linq;
using Perceptron.Domain;
using Perceptron.Persistence;

namespace Perceptron.Application.Commands.Train
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Commande pour entraîner le perceptron
    /// </summary>
    public class TrainCommand : ITrainCommand
    {
        private readonly IBoundaryRepository _boundaryRepository;
        private readonly ILearningSampleRepository _learningSampleRepository;

        public TrainCommand(
            ILearningSampleRepository learningSampleRepository,
            IBoundaryRepository boundaryRepository)
        {
            _learningSampleRepository = learningSampleRepository;
            _boundaryRepository = boundaryRepository;
        }

        public void Execute(TrainingModel model)
        {
            var learningSample = LearningSample.CreateNew(model.Name, model.Skeleton);

            var boundaries = _boundaryRepository.GetAll();

            if (boundaries.All(boundary => boundary.Name != model.Name))
                _boundaryRepository.Add(Boundary.CreateNew(model.Name, learningSample));

            boundaries = _boundaryRepository.GetAll();

            foreach (var boundary in boundaries)
                boundary.Update(learningSample, model.LearningConstant);

            _learningSampleRepository.Add(learningSample);

            _boundaryRepository.Save();
            _learningSampleRepository.Save();
        }

        private void EnsureBoundaryIsAdded(TrainingModel model, Func<Boundary> boundaryFactory)
        {
            if (_boundaryRepository
                .Exists(boundary =>
                    boundary.Name == model.Name))
                return;

            _boundaryRepository.Add(boundaryFactory());
            _boundaryRepository.Save();
        }
    }
}