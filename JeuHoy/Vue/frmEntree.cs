﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
using JeuHoy.Properties;
using Perceptron.Application.Commands.Train;
using Perceptron.Application.Queries.FindBestMatches;

namespace JeuHoy.Vue
{
    /// <summary>
    ///     Auteur: Hugo St-Louis
    ///     Description: Fenêtre princiaple de l'application. Montre les choix à l'utilisateur
    ///     Date: 26/04/2014
    /// </summary>
    public partial class frmEntree : Form
    {
        private readonly JouerMp3 _wmpIntro = new JouerMp3();
        private readonly ITrainCommand _trainCommand;
        private readonly IFindBestMatchesQuery _findBestMatchesQuery;

        /// <summary>
        ///     Constructeur
        /// </summary>
        public frmEntree(ITrainCommand trainCommand, IFindBestMatchesQuery findBestMatchesQuery)
        {
            _trainCommand = trainCommand;
            _findBestMatchesQuery = findBestMatchesQuery;
            InitializeComponent();
            var fichierEntrainement = ConfigurationManager.AppSettings["FichierApp"];

            _wmpIntro.Open(@"./HoyContent/intro.mp3");
            _wmpIntro.Play(true);
        }

        /// <summary>
        ///     Fermeture de la form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmEntree_FormClosing(object sender, FormClosingEventArgs e)
        {
            _wmpIntro.Close();
        }

        /// <summary>
        ///     Ouverture de la fenêtre de jeu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picJouer_Click(object sender, EventArgs e)
        {
            _wmpIntro.Close();

            var f = new frmJeu();
            f.ShowDialog();
            f.Dispose();
            _wmpIntro.Open(@"./HoyContent/intro.mp3");
            _wmpIntro.Play(true);
        }

        /// <summary>
        ///     Comportement lorsque curseur est au dessus d'une image(modifier le curseur)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_MouseHover(object sender, EventArgs e)
        {
            var resources = new ComponentResourceManager(typeof(frmEntree));

            Cursor = Cursors.Hand;
            var p = (PictureBox) sender;
            if (p.Name == "picJouer")
                picJouer.Image = Resources.JouerDessus;
            else if (p.Name == "picEntrainement")
                picEntrainement.Image = Resources.EntrainementDessus;
            else if (p.Name == "picAide")
                picAide.Image = Resources.AideDessus1234;
        }

        /// <summary>
        ///     Comportement lorsque curseur quitte l'image(modifier le curseur)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_MouseLeave(object sender, EventArgs e)
        {
            var resources = new ComponentResourceManager(typeof(frmEntree));
            Cursor = Cursors.Arrow;
            var p = (PictureBox) sender;
            if (p.Name == "picJouer")
                picJouer.Image = Resources.JouerHoy;
            else if (p.Name == "picEntrainement")
                picEntrainement.Image = Resources.Entrainement;
            else if (p.Name == "picAide")
                picAide.Image = Resources.Aide;
        }

        /// <summary>
        ///     Fermeture de la form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picQuitter_Click(object sender, EventArgs e)
        {
            Close();
            _wmpIntro.Close();
        }

        /// <summary>
        ///     Ouverture de la fenêtre d'entrainement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picEntrainement_Click(object sender, EventArgs e)
        {
            _wmpIntro.Close();

            var f = new frmEntrainement(_trainCommand, _findBestMatchesQuery);
            f.ShowDialog();
            f.Dispose();
            _wmpIntro.Open(@"./HoyContent/intro.mp3");
            _wmpIntro.Play(true);
        }

        /// <summary>
        ///     Ouverture de la fenêtre d'aide.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picAide_Click(object sender, EventArgs e)
        {
            var f = new frmAide();
            f.ShowDialog();
            f.Dispose();
        }
    }
}