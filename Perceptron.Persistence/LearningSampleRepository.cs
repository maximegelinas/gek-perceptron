﻿using Perceptron.Domain;

namespace Perceptron.Persistence
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Repository de nos échantillons d'apprentissage
    /// </summary>
    public class LearningSampleRepository : Repository<LearningSample>,
        ILearningSampleRepository
    {
        public LearningSampleRepository(IStorage<LearningSample> storage) :
            base(storage)
        {
        }
    }
}
