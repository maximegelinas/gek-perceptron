﻿using System;

namespace Perceptron.Domain
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Entity qui érite de son interface et qiu crée un ID guid
    /// </summary>
    public abstract class Entity : IEntity
    {
        /// <summary>
        /// constructeur des entity qui crée un ID guid
        /// </summary>
        /// <param name="id"></param>
        protected Entity(Guid id) =>
            Id = id;

        protected Entity() :
            this(Guid.NewGuid())
        {            
        }

        public Guid Id { get; }
    }
}
