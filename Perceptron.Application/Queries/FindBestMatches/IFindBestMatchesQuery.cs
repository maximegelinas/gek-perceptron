﻿using System.Collections.Generic;
using Microsoft.Kinect;

namespace Perceptron.Application.Queries.FindBestMatches
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Interface pour éxécuter cette commande
    /// </summary>
    public interface IFindBestMatchesQuery
    {
        IEnumerable<MatchModel> Execute(Skeleton skeleton);
    }
}