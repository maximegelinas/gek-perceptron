﻿using System;
using System.Collections.Generic;
using Perceptron.Domain;

/// <summary>
/// Auteur: Francois Brisson, Maxime Gélinas
/// Date: 2017/04/21
/// Description: Interface des storage d'entity
/// </summary>
namespace Perceptron.Persistence
{
    public interface IStorage<T> :
        IDisposable
        where T : IEntity
    {
        void Add(T obj);

        void Clear();

        IEnumerable<T> GetAll();

        void Save();

        void Update(T obj);
    }
}