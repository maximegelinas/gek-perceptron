﻿namespace Perceptron.Application.Queries.FindBestMatches
{
    /// <summary>
    /// Auteur: Francois Brisson, Maxime Gélinas
    /// Date: 2017/04/21
    /// Description: Model pour les match
    /// </summary>
    public class MatchModel
    {
        public MatchModel(string foundedName, float strength)
        {
            FoundedName = foundedName;
            Strength = strength;
        }

        public float Strength { get; }

        public string FoundedName { get; }
    }
}
