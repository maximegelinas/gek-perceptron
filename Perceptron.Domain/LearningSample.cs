﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Kinect;

namespace Perceptron.Domain
{
    /// <summary>
    ///     Auteur: Francois Brisson, Maxime Gélinas
    ///     Date: 2017/04/21
    ///     Description: Est une échantillon d'apprentissage qui a un ID
    /// </summary>
    public class LearningSample : Entity
    {
        /// <summary>
        ///     Constructeur des échantillons d'apprentissage
        /// </summary>
        /// <param name="name"></param>
        /// <param name="points"></param>
        private LearningSample(string name, SkeletonPoint[] points)
        {
            if (points.Count() != 20)
                throw new ArgumentException();

            Name = name;
            var values = new List<float>();
            foreach (var point in points)
            {
                values.Add(point.X);
                values.Add(point.Y);
            }
            Values = values.ToArray();
        }

        private LearningSample()
        {
        }

        public float[] Values { get; protected set; }

        public string Name { get; protected set; }

        /// <summary>
        ///     Crée un nouvel échantillon
        /// </summary>
        /// <param name="name"></param>
        /// <param name="skeleton"></param>
        /// <returns></returns>
        public static LearningSample CreateNew(string name, Skeleton skeleton) =>
                new LearningSample(
                    name,
                    skeleton
                        .Joints
                        .Select(joint => joint.Position)
                        .ToRelativePoints(skeleton.Joints[JointType.HipCenter].Position)
                        .ToArray());
        
            
    }
}